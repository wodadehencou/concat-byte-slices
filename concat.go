package concatbyteslices

func Concat(b ...[]byte) []byte {
	var totalLen int
	for _, s := range b {
		totalLen += len(s)
	}
	tmp := make([]byte, totalLen)
	var i int
	for _, s := range b {
		i += copy(tmp[i:], s)
	}
	return tmp
}
